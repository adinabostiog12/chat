﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client
{
    class Player

    {
        public static System.Media.SoundPlayer player;
        private static readonly string[] songs = { "Drake", "Pit", "Salomea", "Miriam" };
        private static readonly string stop = "Stop";

        public static void RecieveMessage(string message)
        {
            if (message.Contains(stop))
                Stop();


            foreach (string song in songs)
                if (message.Contains(song))
                {
                    Play(song);
                    return;
                }
        }

        public static void Stop()
        {
            player?.Stop();
        }

        public static void Play(string song)
        {
            player?.Stop();
            player = new System.Media.SoundPlayer($"../../{song}.wav");
            player?.Play();
        }
    }
}
